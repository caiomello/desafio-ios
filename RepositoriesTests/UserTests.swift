//
//  UserTests.swift
//  Repositories
//
//  Created by Caio Mello on April 19, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import XCTest
@testable import Repositories

class UserTests: XCTestCase {
	func testParse() {
		let dictionary: [String: Any] = ["id": 456,
		                                 "login": "hackerman",
		                                 "avatar_url": "https://i.ytimg.com/vi/KEkrWRHCDQU/maxresdefault.jpg",
		                                 "type": "User"]
		
		XCTAssertNoThrow(try User(dictionary: dictionary))
		
		guard let user = try? User(dictionary: dictionary) else { XCTFail(); return }
		
		XCTAssertEqual(user.identifier, 456)
		XCTAssertEqual(user.name, "hackerman")
		XCTAssertEqual(user.imageURL, "https://i.ytimg.com/vi/KEkrWRHCDQU/maxresdefault.jpg")
		XCTAssertEqual(user.type, .user)
	}
}
