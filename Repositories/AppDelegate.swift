//
//  AppDelegate.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit
import Networking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		setupAppearance()
		setupServices()
		
		return true
	}
}

// MARK: - Setup

extension AppDelegate {
	fileprivate func setupAppearance() {
		if let splitViewController = window?.rootViewController as? UISplitViewController {
			splitViewController.delegate = self
		}
	}
	
	fileprivate func setupServices() {
		APIClient.shared.configuration = self
		APIClient.shared.delegate = self
	}
}

// MARK: - APIClientConfiguration

extension AppDelegate: APIClientConfiguration {
	func baseURL() -> String {
		return "https://api.github.com"
	}
	
	func defaultParameters() -> [String : Any]? {
		return nil
	}
	
	func timeoutInterval() -> TimeInterval {
		return 10
	}
}

// MARK: - APIClientDelegate

extension AppDelegate: APIClientDelegate {
	func didBeginRunningTasks() {
		UIApplication.shared.isNetworkActivityIndicatorVisible = true
	}
	
	func didEndRunningTasks() {
		UIApplication.shared.isNetworkActivityIndicatorVisible = false
	}
}

// MARK: - SplitViewController

extension AppDelegate: UISplitViewControllerDelegate {
	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
		return secondaryViewController is UINavigationController == false
	}
}
