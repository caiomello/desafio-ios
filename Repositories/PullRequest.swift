//
//  PullRequest.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import Foundation
import Networking

struct PullRequest {
	let identifier: Int
	let title: String
	let body: String
	let url: String
	let date: Date
	let user: User
	
	init(dictionary: [String: Any]) throws {
		guard let identifier = dictionary["id"] as? Int else { throw ParsingError.failed(description: "PullRequest - identifier") }
		guard let title = dictionary["title"] as? String else { throw ParsingError.failed(description: "PullRequest - title") }
		guard let body = dictionary["body"] as? String else { throw ParsingError.failed(description: "PullRequest - body") }
		guard let url = dictionary["html_url"] as? String else { throw ParsingError.failed(description: "PullRequest - url") }
		
		DateFormatter.shared.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
		guard let dateString = dictionary["created_at"] as? String else { throw ParsingError.failed(description: "PullRequest - dateString") }
		guard let date = DateFormatter.shared.date(from: dateString) else { throw ParsingError.failed(description: "PullRequest - date") }
		
		guard let userDictionary = dictionary["user"] as? [String: Any] else { throw ParsingError.failed(description: "PullRequest - userDictionary") }
		let user = try User(dictionary: userDictionary)
		
		self.identifier = identifier
		self.title = title
		self.body = body
		self.url = url
		self.date = date
		self.user = user
	}
}

// MARK: - Resources

extension PullRequest {
	struct resource {
		static func repository(_ repository: Repository) -> Resource<[PullRequest]> {
			return Resource<[PullRequest]>(configuration: { () -> ResourceConfiguration in
				let path = "/repos/" + repository.owner.name + "/" + repository.name + "/pulls"
				
				return ResourceConfiguration(method: .get, path: path, parameters: nil, headerFields: nil)
				
			}, parseJSON: { (object) -> [PullRequest]? in
				guard let dictionaries = object as? [[String: Any]] else { throw ParsingError.failed(description: "PullRequest - dictionaries") }
				return try dictionaries.flatMap(PullRequest.init)
			})
		}
	}
}
