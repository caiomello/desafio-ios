//
//  PullRequestsViewController.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit
import Networking
import SafariServices

class PullRequestsViewController: UITableViewController {
	@IBOutlet fileprivate var headerView: PullRequestsTableViewHeaderView!
	
	fileprivate let statusView = ContentStatusView.contentStatusView()
	
	fileprivate var pullRequests: [PullRequest] = []
	
	var repository: Repository?
}

// MARK: - View

extension PullRequestsViewController {
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.backgroundView = statusView
		
		tableView.estimatedRowHeight = tableView.rowHeight
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.tableFooterView = UIView()
		
		if let repository = repository {
			navigationItem.title = repository.name
			requestPullRequests(withRepository: repository)
		}
	}
}

// MARK: - TableView

extension PullRequestsViewController {
	override func numberOfSections(in tableView: UITableView) -> Int {
		if pullRequests.count == 0 {
			statusView.isHidden = false
			return 0
		}
		
		statusView.isHidden = true
		
		return 1
	}
	
	override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let view = headerView
		view?.repository = repository
		return view
	}
	
	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 44
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return pullRequests.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PullRequestTableViewCell
		cell.pullRequest = pullRequests[indexPath.row]
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		presentPullRequest(pullRequests[indexPath.row])
	}
}

// MARK: - Networking

extension PullRequestsViewController {
	fileprivate func requestPullRequests(withRepository repository: Repository) {
		statusView.showActivityIndicator()
		
		APIClient.shared.request(PullRequest.resource.repository(repository)) { (pullRequests, error) in
			self.statusView.hideActivityIndicator()
			
			if let error = error {
				self.statusView.setError(error, buttonTitle: "Try Again", buttonAction: {  self.requestPullRequests(withRepository: repository) })
				self.pullRequests = []
				
			} else if let pullRequests = pullRequests {
				self.pullRequests = pullRequests
				
				if self.pullRequests.count == 0 { self.showEmptyContentText() }
				
			} else {
				self.showEmptyContentText()
				self.pullRequests = []
			}
			
			self.tableView.reloadData()
		}
	}
}

// MARK: - Helpers

extension PullRequestsViewController {
	fileprivate func showEmptyContentText() {
		self.statusView.setTitle("No Pull Requests", subtitle: nil, buttonTitle: nil, buttonAction: nil)
	}
}

// MARK: - Presentation

extension PullRequestsViewController {
	fileprivate func presentPullRequest(_ pullRequest: PullRequest) {
		if let url = URL(string: pullRequest.url) {
			let safariViewController = SFSafariViewController(url: url)
			present(safariViewController, animated: true, completion: nil)
		}
	}
}
