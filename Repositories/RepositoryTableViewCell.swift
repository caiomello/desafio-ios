//
//  RepositoryTableViewCell.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
	@IBOutlet fileprivate var nameLabel: UILabel!
	@IBOutlet fileprivate var descriptionLabel: UILabel!
	
	@IBOutlet fileprivate var forksImageView: UIImageView!
	@IBOutlet fileprivate var starsImageView: UIImageView!
	@IBOutlet fileprivate var forksLabel: UILabel!
	@IBOutlet fileprivate var starsLabel: UILabel!
	
	@IBOutlet fileprivate var ownerImageView: UIImageView!
	@IBOutlet fileprivate var ownerNameLabel: UILabel!
	
	@IBOutlet fileprivate var ownerImageContainerView: CornerRadiusView!
	@IBOutlet fileprivate var ownerImageContainerViewWidthConstraint: NSLayoutConstraint!
	
	var repository: Repository! {
		didSet {
			nameLabel.text = repository.name
			descriptionLabel.text = repository.description ?? "No description."
			
			forksLabel.text = Utilities.formattedCountString(withInteger: repository.numberOfForks)
			starsLabel.text = Utilities.formattedCountString(withInteger: repository.numberOfStars)
			forksImageView.tintColor = forksLabel.textColor
			starsImageView.tintColor = starsLabel.textColor
			
			ownerImageView.setImage(with: repository.owner.imageURL, placeholder: UIImage(color: UIColor.lightGray))
			ownerNameLabel.text = repository.owner.name
			
			switch repository.owner.type {
			case .organization: ownerImageContainerView.cornerRadius = 0
			case .user: ownerImageContainerView.cornerRadius = ownerImageContainerViewWidthConstraint.constant/2
			}
		}
	}
}
