//
//  PullRequestTableViewCell.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
	@IBOutlet fileprivate var titleLabel: UILabel!
	@IBOutlet fileprivate var bodyLabel: UILabel!
	@IBOutlet fileprivate var dateLabel: UILabel!
	@IBOutlet fileprivate var userImageView: UIImageView!
	@IBOutlet fileprivate var usernameLabel: UILabel!
	
	var pullRequest: PullRequest! {
		didSet {
			titleLabel.text = pullRequest.title
			bodyLabel.text = pullRequest.body
			
			DateFormatter.shared.dateFormat = "MMM d, yyyy"
			dateLabel.text = DateFormatter.shared.string(from: pullRequest.date)
			
			userImageView.setImage(with: pullRequest.user.imageURL, placeholder: UIImage(color: UIColor.lightGray))
			usernameLabel.text = pullRequest.user.name
		}
	}
}
