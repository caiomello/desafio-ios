//
//  PullRequestTests.swift
//  Repositories
//
//  Created by Caio Mello on April 19, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import XCTest
@testable import Repositories

class PullRequestTests: XCTestCase {
	func testParse() {
		let dictionary: [String: Any] = ["id": 123,
		                                 "title": "Fix crashes",
		                                 "body": "Remove force-unwrapping of variables",
		                                 "html_url": "url",
		                                 "created_at": "2017-04-19T10:00:00Z",
		                                 "user": ["id": 456,
		                                          "login": "hackerman",
		                                          "avatar_url": "https://i.ytimg.com/vi/KEkrWRHCDQU/maxresdefault.jpg",
		                                          "type": "User"]]
		
		XCTAssertNoThrow(try PullRequest(dictionary: dictionary))
		
		guard let pullRequest = try? PullRequest(dictionary: dictionary) else { XCTFail(); return }
		
		var dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour], from: Date())
		dateComponents.year = 2017
		dateComponents.month = 4
		dateComponents.day = 19
		dateComponents.hour = 10
		
		XCTAssertEqual(pullRequest.identifier, 123)
		XCTAssertEqual(pullRequest.title, "Fix crashes")
		XCTAssertEqual(pullRequest.body, "Remove force-unwrapping of variables")
		XCTAssertEqual(pullRequest.url, "url")
		XCTAssertEqual(pullRequest.date, Calendar.current.date(from: dateComponents))
		XCTAssertEqual(pullRequest.user.identifier, 456)
		XCTAssertEqual(pullRequest.user.name, "hackerman")
		XCTAssertEqual(pullRequest.user.imageURL, "https://i.ytimg.com/vi/KEkrWRHCDQU/maxresdefault.jpg")
		XCTAssertEqual(pullRequest.user.type, .user)
	}
}
