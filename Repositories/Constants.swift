//
//  Constants.swift
//  Repositories
//
//  Created by Caio Mello on April 20, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import Foundation

enum Constant {
	static let numberOfItemsPerPage = 30
}
