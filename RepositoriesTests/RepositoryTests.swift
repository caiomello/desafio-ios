//
//  RepositoryTests.swift
//  Repositories
//
//  Created by Caio Mello on April 19, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import XCTest
@testable import Repositories

class RepositoryTests: XCTestCase {
	func testParse() {
		let dictionary: [String: Any] = ["id": 123,
		                                 "name": "swift",
		                                 "description": "The Swift Programming Language",
		                                 "forks_count": 100,
		                                 "stargazers_count": 2_000,
		                                 "open_issues_count": 40,
		                                 "owner": ["id": 456,
		                                           "login": "Apple",
		                                           "avatar_url": "https://image.freepik.com/free-icon/apple-logo_318-40184.jpg",
		                                           "type": "Organization"]]
		
		XCTAssertNoThrow(try Repository(dictionary: dictionary))
		
		guard let repository = try? Repository(dictionary: dictionary) else { XCTFail(); return }
		
		XCTAssertEqual(repository.identifier, 123)
		XCTAssertEqual(repository.name, "swift")
		XCTAssertEqual(repository.description, "The Swift Programming Language")
		XCTAssertEqual(repository.numberOfForks, 100)
		XCTAssertEqual(repository.numberOfStars, 2000)
		XCTAssertEqual(repository.numberOfOpenIssues, 40)
		XCTAssertEqual(repository.owner.identifier, 456)
		XCTAssertEqual(repository.owner.name, "Apple")
		XCTAssertEqual(repository.owner.imageURL, "https://image.freepik.com/free-icon/apple-logo_318-40184.jpg")
		XCTAssertEqual(repository.owner.type, .organization)
	}
}
