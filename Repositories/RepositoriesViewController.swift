//
//  RepositoriesViewController.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit
import Networking

class RepositoriesViewController: UITableViewController {
	fileprivate let statusView = ContentStatusView.contentStatusView()
	
	fileprivate var repositories: [Repository] = []
	
	fileprivate var language = Language.java {
		didSet {
			navigationItem.title = language.rawValue
		}
	}
	
	fileprivate var page = 1
}

// MARK: - View

extension RepositoriesViewController {
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.backgroundView = statusView
		
		tableView.estimatedRowHeight = tableView.rowHeight
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.tableFooterView = UIView()
		
		requestRepositories(withLanguage: .java, page: 1)
	}
}

// MARK: - TableView

extension RepositoriesViewController {
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		statusView.isHidden = repositories.count > 0
		return repositories.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RepositoryTableViewCell
		cell.repository = repositories[indexPath.row]
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		// Pagination
		let maximumNumberOfItemsForCurrentPage = Constant.numberOfItemsPerPage * page
		
		if repositories.count >= maximumNumberOfItemsForCurrentPage && indexPath.row == (maximumNumberOfItemsForCurrentPage - 10) {
			requestRepositories(withLanguage: language, page: page + 1)
		}
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		performSegue(withIdentifier: "PullRequests", sender: repositories[indexPath.row])
	}
}

// MARK: - Networking

extension RepositoriesViewController {
	fileprivate func requestRepositories(withLanguage language: Language, page: Int) {
		navigationItem.rightBarButtonItem?.isEnabled = false
		statusView.showActivityIndicator()
		
		if page == 1 && language != self.language {
			repositories = []
			tableView.reloadData()
		}
		
		APIClient.shared.request(Repository.resource.language(language, page: page)) { (repositories, error) in
			self.navigationItem.rightBarButtonItem?.isEnabled = true
			self.statusView.hideActivityIndicator()
			
			if let error = error {
				self.statusView.setError(error, buttonTitle: "Try Again", buttonAction: { self.requestRepositories(withLanguage: language, page: page) })
				self.repositories = []
				
			} else if let repositories = repositories {
				if page == 1 {
					self.repositories = repositories
					self.resetScrollPosition()
				} else {
					self.repositories += repositories
				}
				
			} else {
				self.statusView.setTitle("No Repositories", subtitle: nil, buttonTitle: nil, buttonAction: nil)
				self.repositories = []
			}
			
			self.page = page
			
			self.tableView.reloadData()
			
			self.dismissDetailViewController()
		}
	}
}

// MARK: - Helpers

extension RepositoriesViewController {
	fileprivate func resetScrollPosition() {
		if self.tableView.numberOfRows(inSection: 0) > 0 {
			self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
		}
	}
	
	fileprivate func dismissDetailViewController() {
		// Remove pull requests screen when detail view controller is being shown
		if let splitViewController = self.splitViewController, splitViewController.isCollapsed == false {
			self.performSegue(withIdentifier: "Empty", sender: nil)
		}
	}
}

// MARK: - Actions

extension RepositoriesViewController {
	@IBAction fileprivate func refreshControlValueChangedAction(_ sender: UIRefreshControl) {
		sender.endRefreshing()
		requestRepositories(withLanguage: language, page: 1)
	}
	
	@IBAction fileprivate func languageBarButtonAction(_ sender: UIBarButtonItem) {
		presentLanguageSelector()
	}
}

// MARK: - Presentation

extension RepositoriesViewController {
	fileprivate func presentLanguageSelector() {
		let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		
		alertController.addAction(UIAlertAction(title: Language.java.rawValue, style: .default, handler: { (action) in
			self.requestRepositories(withLanguage: .java, page: 1)
			self.language = .java
		}))
		
		alertController.addAction(UIAlertAction(title: Language.swift.rawValue, style: .default, handler: { (action) in
			self.requestRepositories(withLanguage: .swift, page: 1)
			self.language = .swift
		}))
		
		alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		
		alertController.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
		
		present(alertController, animated: true, completion: nil)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let navigationController = segue.destination as? UINavigationController
		let pullRequestsViewController = navigationController?.topViewController as? PullRequestsViewController
		pullRequestsViewController?.repository = sender as? Repository
	}
}
