//
//  UtilitiesTests.swift
//  Repositories
//
//  Created by Caio Mello on April 19, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import XCTest
@testable import Repositories

class UtilitiesTests: XCTestCase {
	func testFormattedCountString() {
		XCTAssertEqual(Utilities.formattedCountString(withInteger: 0), "0")
		XCTAssertEqual(Utilities.formattedCountString(withInteger: 100), "100")
		XCTAssertEqual(Utilities.formattedCountString(withInteger: 1_000), "1k")
		XCTAssertEqual(Utilities.formattedCountString(withInteger: 1_000_000), "1M")
	}
}
