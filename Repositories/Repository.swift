//
//  Repository.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import Foundation
import Networking

enum Language: String {
	case java = "Java"
	case swift = "Swift"
}

struct Repository {
	let identifier: Int
	let name: String
	let description: String?
	let numberOfForks: Int
	let numberOfStars: Int
	let numberOfOpenIssues: Int
	let owner: User
	
	init(dictionary: [String: Any]) throws {
		guard let identifier = dictionary["id"] as? Int else { throw ParsingError.failed(description: "Repository - identifier") }
		guard let name = dictionary["name"] as? String else { throw ParsingError.failed(description: "Repository - name") }
		let description = dictionary["description"] as? String
		guard let numberOfForks = dictionary["forks_count"] as? Int else { throw ParsingError.failed(description: "Repository - numberOfForks") }
		guard let numberOfStars = dictionary["stargazers_count"] as? Int else { throw ParsingError.failed(description: "Repository - numberOfStars") }
		guard let numberOfOpenIssues = dictionary["open_issues_count"] as? Int else { throw ParsingError.failed(description: "Repository - numberOfOpenIssues") }
		
		guard let ownerDictionary = dictionary["owner"] as? [String: Any] else { throw ParsingError.failed(description: "Repository - ownerDictionary") }
		let owner = try User(dictionary: ownerDictionary)
		
		self.identifier = identifier
		self.name = name
		self.description = description
		self.numberOfForks = numberOfForks
		self.numberOfStars = numberOfStars
		self.numberOfOpenIssues = numberOfOpenIssues
		self.owner = owner
	}
}

// MARK: - Resources

extension Repository {
	struct resource {
		static func language(_ language: Language, page: Int) -> Resource<[Repository]> {
			return Resource<[Repository]>(configuration: { () -> ResourceConfiguration in
				let path = "/search/repositories"
				
				let parameters: [String: Any] = ["q": "language:\(language.rawValue)", "sort": "stars", "per_page": Constant.numberOfItemsPerPage, "page": page]
				
				return ResourceConfiguration(method: .get, path: path, parameters: parameters, headerFields: nil)
				
			}, parseJSON: { (object) -> [Repository]? in
				guard let dictionary = object as? [String: Any] else { throw ParsingError.failed(description: "Repository - dictionary") }
				guard let itemDictionaries = dictionary["items"] as? [[String: Any]] else { throw ParsingError.failed(description: "Repository - itemDictionary") }
				return try itemDictionaries.flatMap(Repository.init)
			})
		}
	}
}
