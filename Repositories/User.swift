//
//  User.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import Foundation
import Networking

enum UserType: String {
	case user = "User"
	case organization = "Organization"
}

struct User {
	let identifier: Int
	let name: String
	let imageURL: String
	let type: UserType
	
	init(dictionary: [String: Any]) throws {
		guard let identifier = dictionary["id"] as? Int else { throw ParsingError.failed(description: "User - identifier") }
		guard let name = dictionary["login"] as? String else { throw ParsingError.failed(description: "User - name") }
		guard let imageURL = dictionary["avatar_url"] as? String else { throw ParsingError.failed(description: "User - imageURL") }
		
		guard let typeString = dictionary["type"] as? String else { throw ParsingError.failed(description: "User - typeString") }
		guard let type = UserType(rawValue: typeString) else { throw ParsingError.failed(description: "User - type") }
		
		self.identifier = identifier
		self.name = name
		self.imageURL = imageURL
		self.type = type
	}
}
