//
//  PullRequestsTableHeaderView.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit

class PullRequestsTableViewHeaderView: UIVisualEffectView {
	@IBOutlet fileprivate var titleLabel: UILabel!
	
	var repository: Repository! {
		didSet {
			titleLabel.text = repository.numberOfOpenIssues == 1 ? "1 Open Issue" : "\(repository.numberOfOpenIssues) Open Issues"
		}
	}
}
