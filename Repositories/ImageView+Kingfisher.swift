//
//  ImageView+Kingfisher.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
	func setImage(with url: String?, placeholder: UIImage?) {
		let resource: ImageResource? = {
			if let urlString = url, let url = URL(string: urlString) {
				return ImageResource(downloadURL: url)
			}
			
			return nil
		}()
		
		kf.setImage(with: resource, placeholder: placeholder, options: [.transition(ImageTransition.fade(0.1))], progressBlock: nil, completionHandler: nil)
	}
}
