//
//  UITests.swift
//  Repositories
//
//  Created by Caio Mello on April 19, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import XCTest

class UITests: XCTestCase {
	var app: XCUIApplication!
	
	override func setUp() {
		super.setUp()
		
		continueAfterFailure = false
		
		app = XCUIApplication()
		app.launch()
	}
	
	override func tearDown() {
		app = nil
		super.tearDown()
	}
	
	func testLanguagesSelection() {
		// Rotate to landscape if iPad
		if UIDevice.current.userInterfaceIdiom == .pad {
			XCUIDevice.shared().orientation = .landscapeLeft
		}
		
		var languagesButton = app.navigationBars["Java"].buttons["Languages"]
		XCTAssert(languagesButton.exists)
		expectation(for: NSPredicate(format: "isEnabled == true"), evaluatedWith: languagesButton, handler: nil)
		waitForExpectations(timeout: 10, handler: nil)
		languagesButton.tap()
		
		let javaButton = app.sheets.buttons["Java"]
		let swiftButton = app.sheets.buttons["Swift"]
		
		XCTAssert(javaButton.exists)
		XCTAssert(swiftButton.exists)
		
		swiftButton.tap()
		XCTAssert(app.navigationBars["Swift"].exists)
		
		languagesButton = app.navigationBars["Swift"].buttons["Languages"]
		expectation(for: NSPredicate(format: "isEnabled == true"), evaluatedWith: languagesButton, handler: nil)
		waitForExpectations(timeout: 10, handler: nil)
		languagesButton.tap()
		
		javaButton.tap()
		XCTAssert(app.navigationBars["Java"].exists)
		
		XCTAssertFalse(javaButton.exists)
		XCTAssertFalse(swiftButton.exists)
	}
	
	func testNavigation() {
		// Rotate to landscape if iPad
		if UIDevice.current.userInterfaceIdiom == .pad {
			XCUIDevice.shared().orientation = .landscapeLeft
		}
		
		let repositoryCell = XCUIApplication().tables.staticTexts["elasticsearch"]
		expectation(for: NSPredicate(format: "exists == true"), evaluatedWith: repositoryCell, handler: nil)
		waitForExpectations(timeout: 10, handler: nil)
		XCTAssert(repositoryCell.exists)
		repositoryCell.tap()
		
		XCTAssert(app.navigationBars["elasticsearch"].exists)
		
		let pullRequestCell = app.tables.staticTexts["[Feature] Adding a char_group tokenizer"]
		expectation(for: NSPredicate(format: "exists == true"), evaluatedWith: pullRequestCell, handler: nil)
		waitForExpectations(timeout: 10, handler: nil)
		XCTAssert(pullRequestCell.exists)
		pullRequestCell.tap()
		
		let doneButton = app.buttons["Done"]
		expectation(for: NSPredicate(format: "exists == true"), evaluatedWith: doneButton, handler: nil)
		waitForExpectations(timeout: 10, handler: nil)
		XCTAssert(doneButton.exists)
		doneButton.tap()
		
		XCTAssert(app.navigationBars["elasticsearch"].exists)
	}
}
