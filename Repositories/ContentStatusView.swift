//
//  ContentStatusView.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit
import Networking

class ContentStatusView: UIView {
	@IBOutlet fileprivate var stackView: UIStackView!
	@IBOutlet fileprivate var titleLabel: UILabel!
	@IBOutlet fileprivate var subtitleLabel: UILabel!
	@IBOutlet fileprivate var button: UIButton!
	@IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
	
	fileprivate var title: String? {
		didSet {
			titleLabel.text = title
			titleLabel.isHidden = title == nil
		}
	}
	
	fileprivate var subtitle: String? {
		didSet {
			subtitleLabel.text = subtitle
			subtitleLabel.isHidden = subtitle == nil
		}
	}
	
	fileprivate var buttonTitle: String? {
		didSet {
			button.setTitle(buttonTitle, for: .normal)
			button.isHidden = buttonTitle == nil
		}
	}
	
	fileprivate var buttonAction: (() -> Void)?
	
	fileprivate var error: NetworkingError?
	
	func setError(_ error: NetworkingError?, buttonTitle: String?, buttonAction: (() -> Void)?) {
		setTitle(error?.title, subtitle: error?.message, buttonTitle: buttonTitle, buttonAction: buttonAction)
	}
	
	func setTitle(_ title: String?, subtitle: String?, buttonTitle: String?, buttonAction: (() -> Void)?) {
		self.title = title
		self.subtitle = subtitle
		self.buttonTitle = buttonTitle
		self.buttonAction = buttonAction
	}
}

// MARK: - Init

extension ContentStatusView {
	class func contentStatusView() -> ContentStatusView {
		let view = UINib(nibName: "ContentStatusView", bundle: nil).instantiate(withOwner: nil, options: nil).first as! ContentStatusView
		return view
	}
}


// MARK: - View

extension ContentStatusView {
	override func awakeFromNib() {
		super.awakeFromNib()
		
		title = nil
		subtitle = nil
		buttonTitle = nil
	}
}

// MARK: - ActivityIndicator

extension ContentStatusView {
	func showActivityIndicator() {
		stackView.isHidden = true
		activityIndicator.startAnimating()
	}
	
	func hideActivityIndicator() {
		stackView.isHidden = false
		activityIndicator.stopAnimating()
	}
}

// MARK: - Actions

extension ContentStatusView {
	@IBAction fileprivate func buttonAction(_ sender: UIButton) {
		buttonAction?()
	}
}
