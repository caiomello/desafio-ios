//
//  Utilities.swift
//  Repositories
//
//  Created by Caio Mello on April 18, 2017.
//  Copyright © 2017 Caio Mello. All rights reserved.
//

import UIKit

class Utilities {
	class func formattedCountString(withInteger integer: Int) -> String {
		if integer >= 1_000 && integer < 1_000_000 {
			return "\(Int(Double(integer)/1_000))k"
		} else if integer >= 1_000_000 {
			return "\(Int(Double(integer)/1_000_000))M"
		} else {
			return "\(integer)"
		}
	}
}

// MARK: - DateFormatter

extension DateFormatter {
	@nonobjc static let shared = DateFormatter()
}

// MARK: - IB

@IBDesignable class CornerRadiusView: UIView {
	@IBInspectable var cornerRadius: CGFloat = 0 {
		didSet {
			layer.cornerRadius = cornerRadius
		}
	}
}

// MARK: - Image

extension UIImage {
	convenience init(color: UIColor) {
		let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
		
		UIGraphicsBeginImageContext(rect.size)
		
		let context = UIGraphicsGetCurrentContext()!
		context.setFillColor(color.cgColor)
		context.fill(rect)
		
		let image = UIGraphicsGetImageFromCurrentImageContext()!
		
		UIGraphicsEndImageContext()
		
		self.init(cgImage: image.cgImage!)
	}
}
